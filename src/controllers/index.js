const config = require('../config/config');
const { Pool } = require('pg');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const crypto = require('crypto-js');
const moment = require('moment');
const request = require('request');
const nodemailer = require('nodemailer');
const linkPreview = require('link-preview-js');
const webpush = require('web-push');

const EXPIRE_IN = '3h';
const SECRET_KEY = config.SECRET_KEY;

const pool = new Pool({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database,
    port: config.database.port
});

webpush.setVapidDetails(
    'mailto:travelyperu@gmail.com',
    config.publicKeyPush,
    config.privateKeyPush
);

// const test = async (req, res, next) => {

//     const response_ = await pool.query(`
//             select * from users;
//         `);

//     res.json(response_.rows);
// }

const me = async (req, res, next) => {

    const response = await pool.query(`
        select * from users where user_id=${req.token_data.id}
    `);

    delete response.rows[0]['password'];

    res.json(response.rows[0]);
}

const editProfiler = async (req, res, next) => {

    const insert = [
        req.body.adress,
        req.body.country,
        req.body.doc_nbr,
        req.body.doc_type,
        req.body.lastname,
        req.body.name,
        req.body.phone,
    ];

    try {

        const response = await pool.query(`
            UPDATE users SET 
                adress = '${insert[0]}',
                country = '${insert[1]}',
                doc_nbr = '${insert[2]}',
                doc_type = '${insert[3]}',
                lastname = '${insert[4]}',
                name = '${insert[5]}',
                phone = '${insert[6]}'
            WHERE user_id=${req.token_data.id}
        `);

        res.status(200).json({ msg: 'Se realizaron tus cambios correctamente.' });

    } catch (error) {

        res.status(400).json({ msg: 'Ocurrió un error inesperado, inténtelo nuevamente.' });
    }

}

const sendNotify = async (req, res, next) => {

    const notificationPayload = {
        "notification": {
            "title": "¡Nuevo Mensaje!",
            "body": "Tienes mensajes sin leer",
            "icon": "assets/icons/icon-512x512.png",
            "data": [{ user: 'Mario', id: '2' }],
            "actions": [{
                action: 'showMessage',
                title: 'Ver Mensaje',
            }]
        }
    };

    const resp = await webpush.sendNotification(
        req.body, JSON.stringify(notificationPayload));

    if (resp.statusCode >= 200 && resp.statusCode < 300) {
        res.status(200).json({ message: 'Se envió la notificacion correctamente.' });
    } else {
        res.status(400).json({ message: 'No se pudo enviar la notificación.' })
    }
}

const suscriptionMessage = async (req, res, next) => {

    try {

        const validate = await pool.query(
            `SELECT * FROM suscription WHERE 
                user_id='${req.body.id}' AND
                data='${JSON.stringify(req.body.keySuscription)}'
            `);

        if (validate.rows.length == 0) {

            const response = await pool.query(
                `INSERT INTO 
                suscription(user_id,data,register) 
                VALUES('${req.body.id}', '${JSON.stringify(req.body.keySuscription)}', '${moment().format("YYYY-MM-DD HH:mm:ss")}')
            `);

            const notificationPayload = {
                "notification": {
                    "title": "Travely",
                    "body": "Ahora recibirás las notifiaciones de tus mensajes!",
                    "icon": "assets/icons/icon-512x512.png",
                }
            };

            const resp = await webpush.sendNotification(
                req.body.keySuscription, JSON.stringify(notificationPayload));

            if (resp.statusCode >= 200 && resp.statusCode < 300) {
                res.status(200).json({ message: 'Se envió la notificacion correctamente.' });
            } else {
                res.status(400).json({ message: 'No se pudo enviar la notificación.' })
            }
        } else {

            res.status(200).json({ message: 'Ya se encuentra suscrito.' });
        }

    } catch (error) {
        res.status(400).json({ message: 'No se pudo enviar la notificación.' })
    }
}

const get_offers = async (req, res, next) => {

    const response = await pool.query(`
        select o.*, u.lastname, u.name from offers o
        inner join users u on(u.user_id=o.user_id)
        where o.order_id=${req.params.id}
        order by register desc
    `);

    res.json(response.rows);
}

const get_orders_travaled = async (req, res, next) => {

    if (req.token_data.type == 'V') {
        const response_ = await pool.query(`
            select ofs.offer_id as offer_id_compare, ofs.comision as offer_comision, ofs.comentario as comentario_offer, ofs.delivery_date, ord.*, us.lastname, us.name from orders ord
            left join offers ofs on(ord.order_id=ofs.order_id and ofs.user_id=${req.token_data.id})
            left join users us on(us.user_id=ord.user_id)
            where ord.state='N' or ord.user_offer_id=${req.token_data.id}
            order by ord.register desc
        `);

        res.status(200).json(response_.rows);
    } else if (req.token_data.type == 'C') {
        res.status(403).json({ msg: 'Metodo no perimitido por el usuario.' });
    } else {
        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });
    }


}

const insert_offer = async (req, res, next) => {

    if (req.token_data.type == 'V') {

        const insert = [
            req.body.order_id,
            req.token_data.id,
            req.body.comision_oferta,
            moment(req.body.entrega_oferta).format("YYYY-MM-DD"),
            req.body.comentario_oferta,
            moment().format("YYYY-MM-DD HH:mm:ss"),
        ];

        try {

            const response = await pool.query(
                `INSERT INTO 
                    offers(order_id,user_id,comision,delivery_date,comentario,register) 
                    VALUES($1,$2,$3,$4,$5,$6)
                RETURNING offer_id
                `, insert);

            res.status(200).json({
                order_id: insert[0],
                toUserId: req.body.user_id,
                comentario_offer: insert[4],
                delivery_date: insert[3],
                offer_comision: insert[2],
                offer_id_compare: response.rows[0]['offer_id']
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const edit_offer = async (req, res, next) => {

    if (req.token_data.type == 'V') {

        const data = {
            order_id: req.body.order_id,
            offer_id: req.body.offer_id_compare,
            comision: req.body.comision_oferta,
            delivery_date: moment(req.body.entrega_oferta).format("YYYY-MM-DD"),
            comentario: req.body.comentario_oferta,
            update: moment().format("YYYY-MM-DD HH:mm:ss"),
        };

        try {

            const response = await pool.query(
                `
                    UPDATE offers SET 
                        comision=${data.comision}, 
                        delivery_date='${data.delivery_date}',
                        comentario='${data.comentario}',
                        update='${data.update}'
                    WHERE offer_id=${data.offer_id}
                `);

            res.status(200).json({
                order_id: data.order_id,
                toUserId: req.body.user_id,
                comentario_offer: data.comentario,
                delivery_date: data.delivery_date,
                offer_comision: data.comision,
                offer_id_compare: data.offer_id
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const get_orders_buyer = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        const response = await pool.query(`
        
            select a.*, count(b.order_id) as interesados from orders a
            left join offers b on(a.order_id=b.order_id) 
            where a.user_id=${req.token_data.id}
            group by a.order_id
            order by register desc
        `);

        const result = await search_users(response);

        res.status(200).json(result);

    } else if (req.token_data.type == 'V') {

        res.status(403).json({ msg: 'Metodo no perimitido por el usuario.' });
    } else {

        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });
    }
}

const search_users = async (param) => {

    if (param.rowCount > 0) {

        const data = await param.rows.map(async resp => {

            if (resp.state != 'X' && resp.state != 'N') {

                const response_ = await pool.query(`
           
                    select urs.name, urs.lastname, urs.user_id, ofs.comision as comision_travel from offers ofs
                    left join users urs on urs.user_id=ofs.user_id 
                    where ofs.offer_id=${resp.offer_id}
                `);

                resp.name = await response_.rows[0]['name'];
                resp.lastname = await response_.rows[0]['lastname'];
                resp.user_id_travel = await response_.rows[0]['user_id'];
                resp.comision_travel = await response_.rows[0]['comision_travel'];

                return resp;
            } else {
                return resp;
            }
        });

        return await Promise.all(data);

    } else {

        return [];
    }
}

const getUrlData = async (req, res, next) => {

    linkPreview.getLinkPreview(req.query.urlParam, {
        imagesPropertyType: "og", // fetches only open-graph images
        headers: {
            "user-agent": "googlebot", // fetches with googlebot crawler user agent
            // ...other optional HTTP request headers
        }
    })
        .then((data) => {

            res.status(200).json({ msg: data });
        }).catch(error => {

            res.status(400).json({ msg: 'Url incorrecta.' });
        })
}

const insert_order = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        const insert = [
            req.token_data.id,
            req.body.nombre_pedido,
            req.body.link_pedido,
            req.body.comentario_pedido,
            req.body.precio_pedido,
            req.body.comision_pedido,
            req.body.file,
            'N',
            moment().format("YYYY-MM-DD HH:mm:ss"),
        ];
        try {

            const response = await pool.query(
                `INSERT INTO 
                    orders(user_id,product,link_view,comentario,price,comision,image,state,register) 
                    VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9) 
                RETURNING order_id,user_id,product,link_view,comentario,price,comision,image,state,register
                `, insert);

            response.rows[0]['interesados'] = 0;

            res.status(200).json({ msg: 'Se registró su pedido correctamente.', data: response.rows[0] });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const signup = async (req, res, next) => {

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    const insert = [
        req.body.name,
        req.body.lastname,
        req.body.type,
        req.body.email,
        'P',
        password,
        moment().format("YYYY-MM-DD HH:mm:ss")
    ];
    const response = await pool.query('INSERT INTO users(name,lastname,type,email,state,password,register) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING user_id', insert);

    if (response.rows[0].user_id) {

        const user_id = encodeURIComponent(Buffer.from(await encrypt(response.rows[0].user_id.toString())).toString('base64'));

        const bodyMail = `
            <h2>Hola, ${insert[0]} ${insert[1]}</h2>
            <p>Te has registrado como ${insert[2] == 'V' ? 'Viajero' : 'Comprador'}, para activar tu cuenta has click <a href="${config.url_backend}/api/activate_acount/${user_id}">Aquí</a></p>            
            <p>Una vez que actives tu cuenta, serás redirigido al login el cual ingresarás tu cuenta de correo y tu contraseña elegida.</p>
            <h4>Gracias.</h4>
        `;

        const send = await send_mail(insert[3], 'TRAVELY::CONFIRMACION DE CUENTA', bodyMail);

        if (send.status == 400) {

            res.status(400).json({ msg: 'Correo inválido, inténtelo nuevamente.' });
        } else {

            res.status(200).json({ msg: 'Se envió un correo de confirmación a ' + insert[3] + '.' });
        }
    } else {

        res.status(400).json({ msg: 'Ocurrió un error inesperado, inténtelo nuevamente.' });
    }
}

const activate_acount = async (req, res, next) => {

    try {
        const user_id = await decrypt(Buffer.from(decodeURIComponent(req.params.user_id), 'base64').toString('ascii'));

        const response = await pool.query(`UPDATE users SET state='A' WHERE user_id='${user_id}'`);

        // res.status(200).json({ msg: 'Se activó su cuenta correctamente.' });
        res.redirect(config.url_frontend + '/login')

    } catch (error) {

        res.status(400).json({ msg: 'Token de seguridad inválido.' });
    }
}

const signin = async (req, res, next) => {

    const values = [req.body.email, req.body.password];
    const response = await pool.query('SELECT * FROM users WHERE email = $1', [values[0]]);

    if (response.rows.length == 0) {

        res.status(401).json({ msg: "Usuario y/o contraseña son inválidas." });
    } else {

        const valid = await bcrypt.compare(values[1], response.rows[0].password);

        if (!valid) {
            res.status(401).json({ msg: "Usuario y/o contraseña son inválidas." });
        } else if (response.rows[0]['state'] == 'P') {
            res.status(401).json({ msg: "Debe confirmar su cuenta por correo." });
        } else {
            let obj_token = {
                id: response.rows[0]['user_id'],
                user: response.rows[0]['email'],
                name: response.rows[0]['name'],
                lastname: response.rows[0]['lastname'],
                type: response.rows[0]['type']
            };

            const token = jwt.sign(obj_token, SECRET_KEY, { expiresIn: EXPIRE_IN });

            res.status(200).json({ token });
        }
    }
}

const users = async (req, res, next) => {

    const response = await pool.query(`SELECT name, lastname, email ,banco, nrocuenta, phone, doc_type, doc_nbr, country, adress FROM users where user_id=${req.token_data.id}`);
    // const response = await pool.query('TRUNCATE TABLE users');
    res.status(200).json(response.rows[0]);
}

const user = async (req, res, next) => {

    try {

        const response = await pool.query(`
        UPDATE users set name='${req.body.name ? req.body.name : ''}',banco='${req.body.banco ? req.body.banco : ''}', nrocuenta='${req.body.nrocuenta ? req.body.nrocuenta : ''}', lastname='${req.body.lastname ? req.body.lastname : ''}', email='${req.body.email ? req.body.email : ''}', phone='${req.body.phone ? req.body.phone : ''}', doc_type='${req.body.doc_type ? req.body.doc_type : ''}', doc_nbr='${req.body.doc_nbr ? req.body.doc_nbr : ''}', country='${req.body.country ? req.body.country : ''}', adress='${req.body.adress ? req.body.adress : ''}' 
        where user_id=${req.token_data.id}`);
        res.status(200).json('se guardó correctamente.');
    } catch (error) {

        res.status(400).json('Ocurrió un error inesperado.');
    }


}

const verifyemail = async (req, res, next) => {

    const email = req.body.email;

    if (!email) res.status(200).json({ msg: 'email inválido.', status: 400 });

    const response = await pool.query('SELECT * FROM users WHERE email = $1', [email]);

    if (response.rows.length > 0) res.status(200).json({ msg: 'Ya existe el email.', status: 400 })

    else res.status(200).json({ msg: 'Email válido.' })
}

const acept_offers = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        try {

            const response = await pool.query(
                `   
                    UPDATE orders set offer_id = ${req.body.offer_id}, state='A', user_offer_id=${req.body.user_id}
                    WHERE order_id=${req.body.order_id}
                `);

            res.status(200).json({
                comision_travel: req.body.comision, //nuevo
                lastname: req.body.lastname, //nuevo
                name: req.body.name, //nuevo
                offer_id: req.body.offer_id,
                state: "A",
                user_id_travel: req.body.user_id,
                order_id: req.body.order_id
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const confirm_order = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        try {

            const response = await pool.query(
                `   
                    UPDATE orders set state='R', comentario = '${req.body.text_message}'
                    WHERE order_id='${req.body.order_id}'
                `);


            res.status(200).json({ msg: 'Confirmado correctamente.' });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const validatePago = async (req, res, next) => {

    try {
        const dataProduct = await decrypt(Buffer.from(decodeURIComponent(req.params.order_id), 'base64').toString('ascii'));
        const order = JSON.parse(dataProduct);

        const response = await pool.query(
            `   
                    UPDATE orders set state='P'
                    WHERE order_id='${order.order_id}'
                `);

        msg = {
            status: 200,
            title: 'Confirmación de pago',
            msg: 'Se confirmó el pago correctamente.'
        }

    } catch (e) {

        msg = {
            status: 400,
            title: 'Confirmación de pago',
            msg: 'Algo salió mal, vuelva a confirmar el pago.'
        }
    }

    const user_id = encodeURIComponent(await encrypt(JSON.stringify(msg).toString()));
    // console.log(user_id);
    res.redirect(config.url_frontend + '/messageGet/' + user_id)
}

const cancel_order = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        try {

            const response = await pool.query(
                `   
                    UPDATE orders set state='X'
                    WHERE order_id=${req.body.order_id}
                `);

            res.status(200).json({ msg: 'Se canceló su pedido correctamente.', data: { order_id: req.body.order_id, state: 'X' } });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const encrypt = async (value) => {

    return crypto.AES.encrypt(value, config.secretKeyForms).toString();
}

const decrypt = async (textToDecrypt) => {

    return crypto.AES.decrypt(textToDecrypt, config.secretKeyForms).toString(crypto.enc.Utf8);
}

const confirm_pedido = async (req, res, next) => {

    if (req.token_data.type == 'C') {

        try {

            const dec = await decrypt(req.body.data_pedido);

            let data = JSON.parse(dec);

            var options = {
                'method': 'POST',
                'url': 'https://secure.culqi.com/v2/tokens',
                'headers': {
                    'Authorization': 'Bearer ' + config.llavePublicaCulqui,
                    'Content-Type': ['application/json']
                },
                body: JSON.stringify({
                    "card_number": data.tarjetaNumber.replace(/ /g, ''),
                    "cvv": data.cvv_key,
                    "expiration_month": data.mes_key,
                    "expiration_year": data.anio_key,
                    "email": data.correo
                })
            };

            const responseToken = await request(options, async function (error, response) {

                let resp = JSON.parse(response.body);

                if (error) {

                    res.status(400).json(resp);
                } else {

                    if (resp.object == 'error') {

                        res.status(400).json(resp);
                    } else {

                        //guardar token en db

                        var options1 = {
                            'method': 'POST',
                            'url': 'https://api.culqi.com/v2/charges',
                            'headers': {
                                'Content-Type': ['application/json'],
                                'Authorization': 'Bearer ' + config.llavePrivadaCulqui
                            },
                            body: JSON.stringify({
                                "amount": data.total.replace(/[,.]/g, ''),
                                "currency_code": "PEN",
                                "email": data.correo,
                                "source_id": resp.id
                            })
                        };

                        request(options1, async function (error1, response1) {

                            let resp1 = JSON.parse(response1.body);

                            if (error1) {

                                res.status(400).json(resp1);
                            } else {

                                if (resp1.object == 'error') {

                                    res.status(400).json(resp1);

                                } else {
                                    //guardar cargo en db
                                    //actualizar pedido
                                    const response_db1 = await pool.query(
                                        `   
                                            UPDATE orders set state='A'
                                            WHERE order_id=${data.order_id}
                                        `);

                                    const response_db2 = await pool.query(
                                        `   
                                            INSERT INTO payments(order_id, token_create, cargo_create, status_token, status_cargo, register_token, register_cargo) 
                                            VALUES (${data.order_id}, '${JSON.stringify(resp)}', '${JSON.stringify(resp1)}' , 'T', 'T', '${moment().format("YYYY-MM-DD HH:mm:ss")}', '${moment().format("YYYY-MM-DD HH:mm:ss")}')
                                        `);

                                    // console.log(data.order_id);
                                    res.status(200).json({ msg: 'Confirmado correctamente.' });
                                }
                            }
                        });
                    }
                }
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const insert_chat = async (req, res, next) => {

    try {

        const response = await pool.query(`
            INSERT INTO 
                chats(order_id ,user_id ,message ,state ,register, from_user_id) 
                VALUES(${req.body.order_id},${req.body.toUserId},'${req.body.message}','N','${moment().format("YYYY-MM-DD HH:mm:ss")}', ${req.body.user_id}) 
            RETURNING chat_id, register, state
        `);

        res.status(200).json({
            chat_id: response.rows[0]['chat_id'],
            message: req.body.message,
            order_id: req.body.order_id,
            register: response.rows[0]['register'],
            state: response.rows[0]['state'],
            user_id: req.body.user_id,
        });

    } catch (e) {

        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

        console.log(e);
    }
}

const confirmPay = async (req, res, next) => {
    try {

        const orderObj = encodeURIComponent(Buffer.from(await encrypt(JSON.stringify(req.body).toString())).toString('base64'));

        const response = await pool.query(`UPDATE orders SET nro_operation='${req.body.nrpOperacion}' WHERE order_id='${req.body.order_id}'`);
        const response1 = await pool.query(`
            select u.name, u.lastname, u.type, u.email, u.phone from orders o
                inner join users u on(o.user_id=u.user_id)
                    where o.order_id = ${req.body.order_id};
        `);

        const response2 = await pool.query(`
            select u.name, u.lastname, u.type, u.email, u.phone from users u
            where u.user_id = ${req.body.user_id_travel}
        `);

        const bodyMail = `
            <h2>
                Se ha realizado el pago del pedido Nro ${req.body.order_id} con número de 
                operación bancaria: ${req.body.nrpOperacion}.
                <br>
                Producto: ${req.body.product}<br>
                Precio: ${req.body.precio}<br>
                Comision: $ ${req.body.comision}<br>
                Comision de aplicación: $ ${req.body.comision_app}<br>
                Total: $ ${req.body.totalAmount}<br>
                <br>
                **************************
                <br>
                Comprador: ${response1.rows[0]['name']} ${response1.rows[0]['lastname']}<br>
                Celular: ${response1.rows[0]['phone']}<br>
                Correo: ${response1.rows[0]['email']}
                <br>
                **************************
                <br>
                Viajero: ${response2.rows[0]['name']} ${response2.rows[0]['lastname']}<br>
                Celular: ${response2.rows[0]['phone']}<br>
                Correo: ${response2.rows[0]['email']}
            </h2>

            <a href="${config.url_backend}/api/validatePago/${orderObj}">Pago exitoso</a>
        `;

        send_mail('travelyperu@gmail.com', 'TRAVELY::CONFIRMACIÓN DE PAGO', bodyMail);

        res.status(200).json({
            nrpOperacion: req.body.nrpOperacion,
            order_id: req.body.order_id,
            user_id_travel: req.body.user_id_travel,
            msg: 'Se confirmará el pago en el transcurso de 24 horas despues de haber realizado el pago.'
        });

    } catch (error) {

        console.log(error);

        res.status(400).json({ msg: 'Token de seguridad inválido.' });
    }
}

const confirmEntrega = async (req, res, next) => {
    try {

        const response = await pool.query(`UPDATE orders SET state='C', comentario_mejora='${req.body.comentario}' WHERE order_id='${req.body.order_id}'`);

        res.status(200).json({
            user_id_travel: req.body.user_id_travel,
            order_id: req.body.order_id,
            state: 'C',
            comentario_mejora: req.body.comentario,
            msg: 'Gracias por usar Travely.'
        });

    } catch (error) {

        res.status(400).json({ msg: 'Token de seguridad inválido.' });
    }
}

const get_chat = async (req, res, next) => {

    try {
        const response = await pool.query(`
            select * from chats ch 
            where ch.order_id='${req.body.order_id}' 
            and ((ch.user_id=${req.token_data.id} and ch.from_user_id=${req.body.toUserId}) 
            or (ch.from_user_id=${req.token_data.id} and ch.user_id=${req.body.toUserId}))
        `);

        res.status(200).json(response.rows);

    } catch (e) {

        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

        console.log(e);
    }
}

const forgotPassword = async (req, res, next) => {
    // console.log();
    req.body.maxDate = moment().format("YYYY-MM-DD");
    const user_id = encodeURIComponent(Buffer.from(await encrypt(JSON.stringify(req.body).toString())).toString('base64'));
    const bodyMail = `
        <h2>Hola, ${req.body.nombre} ${req.body.apellido}</h2>
        <p>Has solicitado el cambio de contraseña, para cambiar la contraseña, has click <a href="${config.url_backend}/api/changePassword/${user_id}">Aquí</a></p>            
        <p>Si no has sido tu, ignora este mensaje.</p>
        <h4>Gracias.</h4>
    `;
    const send = await send_mail(req.body.email, 'TRAVELY::RECUPERACIÓN DE CUENTA', bodyMail);

    if (send.status == 400) {

        res.status(400).json({ msg: 'Correo inválido, inténtelo nuevamente.' });
    } else {

        res.status(200).json({ msg: 'Se envió un correo de confirmación de cambio de contraseña a ' + req.body.email + ' gracias.' });
    }
}

const changePassword = async (req, res, next) => {

    let msg = '';

    try {
        const user_id_d = await decrypt(Buffer.from(decodeURIComponent(req.params.user_id), 'base64').toString('ascii'));
        const newData = JSON.parse(user_id_d);
        const salt = await bcrypt.genSalt(10);
        const newPassword = await bcrypt.hash(newData.newPassword, salt);

        try {

            const response = await pool.query(`UPDATE users SET password='${newPassword}' WHERE email='${newData.email}'`);

            msg = {
                status: 200,
                title: 'Solicitud de cambio de contraseña',
                msg: 'Se cambió su contraseña correctamente.'
            }
        } catch (error1) {

            msg = {
                status: 400,
                title: 'Solicitud de cambio de contraseña',
                msg: 'Algo salió mal, vuelva a solicitar su cambio de contraseña.'
            }
        }

    } catch (error2) {
        msg = {
            status: 400,
            msg: 'Algo salió mal, vuelva a solicitar su cambio de contraseña.'
        }
    }

    const user_id = encodeURIComponent(await encrypt(JSON.stringify(msg).toString()));
    // console.log(user_id);
    res.redirect(config.url_frontend + '/messageGet/' + user_id)
}

const send_mail = async (to, asunto, body) => {

    const transporter = await nodemailer.createTransport({
        host: config.servesmpt,
        secure: false,
        port: 587,
        auth: {
            user: config.llavesmptuser,
            pass: config.llavesmptpass
        }
    });

    const mailOptions = {
        from: 'travelyperu@gmail.com',
        to: to,
        subject: asunto,
        html: body
    };

    let obj;

    return new Promise(resolve => {

        transporter.sendMail(mailOptions, (error, info) => {

            if (error) {

                resolve({
                    status: 400,
                    msg: error
                });
            } else {

                resolve({
                    status: 200,
                    msg: info
                })
            }
        })

    });
}

module.exports = {
    signup,
    signin,
    me,
    users,
    verifyemail,
    insert_order,
    get_orders_buyer,
    get_orders_travaled,
    insert_offer,
    edit_offer,
    get_offers,
    acept_offers,
    confirm_order,
    cancel_order,
    confirm_pedido,
    insert_chat,
    get_chat,
    activate_acount,
    getUrlData,
    user,
    confirmPay,
    confirmEntrega,
    forgotPassword,
    changePassword,
    suscriptionMessage,
    sendNotify,
    validatePago,
    editProfiler
}