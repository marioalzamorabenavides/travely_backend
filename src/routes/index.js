const { Router } = require('express');
const router = Router();
const verifytoken = require('../controllers/verifytoken');

const controller = require('../controllers/index');

router.post('/signup', controller.signup);

router.post('/signin', controller.signin);

router.get('/users', verifytoken, controller.users);

router.post('/user', verifytoken, controller.user);

router.get('/getUrlData', verifytoken, controller.getUrlData);

router.get('/activate_acount/:user_id', controller.activate_acount);

router.get('/me', verifytoken, controller.me);

router.post('/verifyemail', controller.verifyemail);

router.post('/insert_order', verifytoken, controller.insert_order);

router.get('/get_orders_buyer', verifytoken, controller.get_orders_buyer);

router.get('/get_orders_travaled', verifytoken, controller.get_orders_travaled);

router.post('/editProfiler', verifytoken, controller.editProfiler);

router.get('/changePassword/:user_id', controller.changePassword);

router.get('/validatePago/:order_id', controller.validatePago);

router.post('/insert_offer', verifytoken, controller.insert_offer);

router.post('/edit_offer', verifytoken, controller.edit_offer);

router.post('/forgotPassword', controller.forgotPassword);

router.get('/get_offers/:id', verifytoken, controller.get_offers);

router.put('/acept_offers', verifytoken, controller.acept_offers);

router.put('/confirm_order', verifytoken, controller.confirm_order);

router.put('/cancel_order', verifytoken, controller.cancel_order);

router.put('/confirm_pedido', verifytoken, controller.confirm_pedido);

router.put('/confirmPay', verifytoken, controller.confirmPay);

router.put('/confirmEntrega', verifytoken, controller.confirmEntrega);

router.post('/insert_chat', verifytoken, controller.insert_chat);

router.post('/getChatOrder', verifytoken, controller.get_chat);

router.post('/suscriptionMessage', controller.suscriptionMessage);

router.post('/sendNotify', controller.sendNotify);

module.exports = router;