PGDMP                     
    x            travely    12.4    12.4 .    A           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            B           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            C           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            D           1262    16448    travely    DATABASE     �   CREATE DATABASE travely WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE travely;
                postgres    false            �            1259    16449    chats    TABLE     �   CREATE TABLE public.chats (
    chat_id integer NOT NULL,
    order_id integer,
    user_id integer,
    message text,
    state "char",
    register timestamp with time zone,
    from_user_id integer
);
    DROP TABLE public.chats;
       public         heap    postgres    false            �            1259    16455    chats_chat_id_seq    SEQUENCE     �   CREATE SEQUENCE public.chats_chat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.chats_chat_id_seq;
       public          postgres    false    202            E           0    0    chats_chat_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.chats_chat_id_seq OWNED BY public.chats.chat_id;
          public          postgres    false    203            �            1259    16457    offers    TABLE       CREATE TABLE public.offers (
    offer_id integer NOT NULL,
    order_id integer,
    user_id integer,
    comision double precision,
    delivery_date date,
    comentario character varying(255),
    register timestamp with time zone,
    update timestamp with time zone
);
    DROP TABLE public.offers;
       public         heap    postgres    false            �            1259    16463    offers_offer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.offers_offer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.offers_offer_id_seq;
       public          postgres    false    204            F           0    0    offers_offer_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.offers_offer_id_seq OWNED BY public.offers.offer_id;
          public          postgres    false    205            �            1259    16465    orders    TABLE     �  CREATE TABLE public.orders (
    order_id integer NOT NULL,
    user_id integer,
    offer_id integer,
    product character varying(255),
    link_view text,
    price double precision,
    comision double precision,
    image text,
    state character(1),
    register timestamp with time zone,
    comentario character varying(255),
    nro_operation character varying(150),
    comentario_mejora text
);
    DROP TABLE public.orders;
       public         heap    postgres    false            �            1259    16471    orders_order_id_seq    SEQUENCE     �   CREATE SEQUENCE public.orders_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.orders_order_id_seq;
       public          postgres    false    206            G           0    0    orders_order_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.orders_order_id_seq OWNED BY public.orders.order_id;
          public          postgres    false    207            �            1259    16473    payments    TABLE     �  CREATE TABLE public.payments (
    payment_id integer NOT NULL,
    order_id integer,
    token_create text,
    cargo_create text,
    devolver_cargo text,
    status_token character(1),
    status_cargo character(1),
    status_devolver character(1),
    register_token timestamp with time zone,
    register_cargo timestamp with time zone,
    register_devolver timestamp with time zone
);
    DROP TABLE public.payments;
       public         heap    postgres    false            �            1259    16479    payments_payment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payments_payment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.payments_payment_id_seq;
       public          postgres    false    208            H           0    0    payments_payment_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.payments_payment_id_seq OWNED BY public.payments.payment_id;
          public          postgres    false    209            �            1259    16540    suscription    TABLE     �   CREATE TABLE public.suscription (
    suscription_id integer NOT NULL,
    user_id integer NOT NULL,
    data text NOT NULL,
    register timestamp with time zone NOT NULL
);
    DROP TABLE public.suscription;
       public         heap    postgres    false            �            1259    16538    suscription_suscription_id_seq    SEQUENCE     �   CREATE SEQUENCE public.suscription_suscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.suscription_suscription_id_seq;
       public          postgres    false    213            I           0    0    suscription_suscription_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.suscription_suscription_id_seq OWNED BY public.suscription.suscription_id;
          public          postgres    false    212            �            1259    16481    users    TABLE     �  CREATE TABLE public.users (
    user_id integer NOT NULL,
    name character varying(100),
    lastname character varying(255),
    type "char",
    email character varying(255),
    phone character varying(15),
    doc_type character varying(15),
    doc_nbr character varying(15),
    country character varying(15),
    adress character varying(255),
    state "char",
    password character varying(255),
    register timestamp with time zone
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16487    users_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.users_user_id_seq;
       public          postgres    false    210            J           0    0    users_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;
          public          postgres    false    211            �
           2604    16489    chats chat_id    DEFAULT     n   ALTER TABLE ONLY public.chats ALTER COLUMN chat_id SET DEFAULT nextval('public.chats_chat_id_seq'::regclass);
 <   ALTER TABLE public.chats ALTER COLUMN chat_id DROP DEFAULT;
       public          postgres    false    203    202            �
           2604    16490    offers offer_id    DEFAULT     r   ALTER TABLE ONLY public.offers ALTER COLUMN offer_id SET DEFAULT nextval('public.offers_offer_id_seq'::regclass);
 >   ALTER TABLE public.offers ALTER COLUMN offer_id DROP DEFAULT;
       public          postgres    false    205    204            �
           2604    16491    orders order_id    DEFAULT     r   ALTER TABLE ONLY public.orders ALTER COLUMN order_id SET DEFAULT nextval('public.orders_order_id_seq'::regclass);
 >   ALTER TABLE public.orders ALTER COLUMN order_id DROP DEFAULT;
       public          postgres    false    207    206            �
           2604    16492    payments payment_id    DEFAULT     z   ALTER TABLE ONLY public.payments ALTER COLUMN payment_id SET DEFAULT nextval('public.payments_payment_id_seq'::regclass);
 B   ALTER TABLE public.payments ALTER COLUMN payment_id DROP DEFAULT;
       public          postgres    false    209    208            �
           2604    16543    suscription suscription_id    DEFAULT     �   ALTER TABLE ONLY public.suscription ALTER COLUMN suscription_id SET DEFAULT nextval('public.suscription_suscription_id_seq'::regclass);
 I   ALTER TABLE public.suscription ALTER COLUMN suscription_id DROP DEFAULT;
       public          postgres    false    212    213    213            �
           2604    16493    users user_id    DEFAULT     n   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    211    210            3          0    16449    chats 
   TABLE DATA           c   COPY public.chats (chat_id, order_id, user_id, message, state, register, from_user_id) FROM stdin;
    public          postgres    false    202   z6       5          0    16457    offers 
   TABLE DATA           t   COPY public.offers (offer_id, order_id, user_id, comision, delivery_date, comentario, register, update) FROM stdin;
    public          postgres    false    204   �6       7          0    16465    orders 
   TABLE DATA           �   COPY public.orders (order_id, user_id, offer_id, product, link_view, price, comision, image, state, register, comentario, nro_operation, comentario_mejora) FROM stdin;
    public          postgres    false    206   �6       9          0    16473    payments 
   TABLE DATA           �   COPY public.payments (payment_id, order_id, token_create, cargo_create, devolver_cargo, status_token, status_cargo, status_devolver, register_token, register_cargo, register_devolver) FROM stdin;
    public          postgres    false    208   �6       >          0    16540    suscription 
   TABLE DATA           N   COPY public.suscription (suscription_id, user_id, data, register) FROM stdin;
    public          postgres    false    213   �6       ;          0    16481    users 
   TABLE DATA           �   COPY public.users (user_id, name, lastname, type, email, phone, doc_type, doc_nbr, country, adress, state, password, register) FROM stdin;
    public          postgres    false    210   7       K           0    0    chats_chat_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.chats_chat_id_seq', 1, false);
          public          postgres    false    203            L           0    0    offers_offer_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.offers_offer_id_seq', 1, false);
          public          postgres    false    205            M           0    0    orders_order_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.orders_order_id_seq', 1, false);
          public          postgres    false    207            N           0    0    payments_payment_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.payments_payment_id_seq', 1, false);
          public          postgres    false    209            O           0    0    suscription_suscription_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.suscription_suscription_id_seq', 1, false);
          public          postgres    false    212            P           0    0    users_user_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.users_user_id_seq', 1, false);
          public          postgres    false    211            �
           2606    16495    chats chats_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.chats
    ADD CONSTRAINT chats_pkey PRIMARY KEY (chat_id);
 :   ALTER TABLE ONLY public.chats DROP CONSTRAINT chats_pkey;
       public            postgres    false    202            �
           2606    16497    offers offer_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offer_pkey PRIMARY KEY (offer_id);
 ;   ALTER TABLE ONLY public.offers DROP CONSTRAINT offer_pkey;
       public            postgres    false    204            �
           2606    16499    orders orders_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (order_id);
 <   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_pkey;
       public            postgres    false    206            �
           2606    16501    payments payments_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (payment_id);
 @   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_pkey;
       public            postgres    false    208            �
           2606    16548    suscription suscription_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.suscription
    ADD CONSTRAINT suscription_pkey PRIMARY KEY (suscription_id);
 F   ALTER TABLE ONLY public.suscription DROP CONSTRAINT suscription_pkey;
       public            postgres    false    213            �
           2606    16503    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    210            3      x������ � �      5      x������ � �      7      x������ � �      9      x������ � �      >      x������ � �      ;      x������ � �     